import React from 'react';

import { Button } from './components/Button';
import { Preloader } from './components/Preloader';
import { asyncFn } from './helper';
import './styles.css'

export class Root extends React.Component {
  constructor(props) {
    super(props);

    this.statusRef = React.createRef();

    this.state = {
      content: null,
      showPreloader: false,
    };
  }

  componentDidMount() {
    this.setState({ showPreloader: true });

    asyncFn().then((content) => {
      this.setState({ content });
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.statusRef.current && prevState.showPreloader !== this.state.showPreloader) {
      setTimeout(() => {
        this.statusRef.current.remove();
      }, 600)
    }
  }

  hidePreloader = () => {
    this.setState({ showPreloader: false });
  };

  render() {
    const { showPreloader, content } = this.state;

    if (!(showPreloader || content)) {
      return <span>Меня не должно быть видно 🥸</span>;
    }

    return (
      <div className="Root">
        {showPreloader ? <Preloader /> : content}

        <Button color="blue" onClick={this.hidePreloader}>
          Хватит крутить
        </Button>

        {content && (
          <div className="Root-status" ref={this.statusRef}>
            Контент есть, покажите мне его полностью. Нажми на кнопку
          </div>
        )}
      </div>
    );
  }
}

export default Root;
