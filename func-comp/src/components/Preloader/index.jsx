import React from 'react';

import './styles.css';

export class Preloader extends React.PureComponent {
  componentWillUnmount() {
    alert('Preloader: я пошел - сча будет контент');
  }

  render() {
    const { size } = this.props;

    return (
      <div style={{ width: `${size}px`, height: `${size}px` }} className="Preloader-component" />
    );
  }
}

Preloader.defaultProps = { size: 50 };
